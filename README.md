# Learning about Wikis

This is an easy to find page for any users on the internet that need to know how to write Wiki pages for *GitLab* not _Github_ environments, in the markdown language. 

## What is a Wiki
A wiki (sometimes spelled "Wiki") is a server program that allows users to collaborate in forming the content of a Web site. The term comes from the word "wikiwiki," which means "fast" in the Hawaiian language. A wiki provides a simplified interface. It is not necessary to know HTML.

## Anthology of Markdown (md) References

[Basic writing and formatting syntax](https://docs.github.com/en/get-started/writing-on-github/getting-started-with-writing-and-formatting-on-github/basic-writing-and-formatting-syntax)<br>
[Quoting code](https://docs.github.com/en/get-started/writing-on-github/getting-started-with-writing-and-formatting-on-github/basic-writing-and-formatting-syntax#quoting-code)<br>
[Markdown syntax guide](https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)<br>
[Make a README](https://www.makeareadme.com/)<br>
[Tables](https://rdmd.readme.io/docs/tables)<br>
